package br.com.ozcorp;

/**
 * 
 * @author Matheus Jos�
 *
 */

public class Cargo {

	//Atributos
	String titulo;
	double salarioBase;

	// Constructor
	public Cargo(String titulo, double salarioBase) {
		this.titulo = titulo;
		this.salarioBase = salarioBase;
	}

	// Get & Set
	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public double getSalarioBase() {
		return salarioBase;
	}

	public void setSalarioBase(double salarioBase) {
		this.salarioBase = salarioBase;
	}

}
