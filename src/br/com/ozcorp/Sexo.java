package br.com.ozcorp;

/**
 * 
 * @author Matheus Jos�
 *
 */

public enum Sexo {
	//Enumerador
	MASCULINO("Masculino"), FEMININO("Feminino"), OUTRO("Outro");

	public String nomeDoSexo;

	//Constructor
	Sexo(String nomeDoSexo) {
		this.nomeDoSexo = nomeDoSexo;
	}
}
