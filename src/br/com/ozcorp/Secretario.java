package br.com.ozcorp;

/**
 * 
 * @author Matheus Jos�
 *
 */

public class Secretario extends Funcionario {

	public static final int NIVELACESSO = 1;

	//Heran�a
	protected Secretario(Departamento departamento, Sexo sexo, TipoSanguineo tipoSanguineo, int nivelAcesso,
			String nome, String rg, String cpf, String email, String senha, String matricula) {
		super(departamento, sexo, tipoSanguineo, nivelAcesso, nome, rg, cpf, email, senha, matricula);

	}

}
