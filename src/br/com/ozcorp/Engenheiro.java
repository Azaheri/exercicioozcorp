package br.com.ozcorp;

/**
 * 
 * @author Matheus Jos�
 *
 */

public class Engenheiro extends Funcionario {

	public static final int NIVELACESSO = 3;

	//Heran�a
	protected Engenheiro(Departamento departamento, Sexo sexo, TipoSanguineo tipoSanguineo, int nivelAcesso,
			String nome, String rg, String cpf, String email, String senha, String matricula) {
		super(departamento, sexo, tipoSanguineo, nivelAcesso, nome, rg, cpf, email, senha, matricula);

	}

}
