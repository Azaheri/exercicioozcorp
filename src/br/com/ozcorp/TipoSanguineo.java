package br.com.ozcorp;

/**
 * 
 * @author Matheus Jos�
 *
 */

public enum TipoSanguineo {
	
	//Enumerador
	O_NEGATIVO("Rh O-"), A_NEGATIVO("Rh A-"), B_NEGATIVO("Rh B-"), AB_NEGATIVO("Rh AB-"), O_POSITIVO(
			"Rh O+"), A_POSITIVO("Rh A+"), B_POSITIVO("Rh B+"), AB_POSITIVO("Rh AB+");

	String sangue;

	//Constructor
	private TipoSanguineo(String sangue) {
		this.sangue = sangue;
	}

}
