package br.com.ozcorp;

/**
 * 
 * @author Matheus Jos�
 *
 */

public class FichaFuncionario {

	// Recebe dados
	public static void main(String[] args) {

		Funcionario funcionario1 = new Funcionario(
				new Departamento("Departamento de Inform�tica", "DI", new Cargo("Analista", 7396.00)), Sexo.MASCULINO,
				TipoSanguineo.O_NEGATIVO, 4, "Matheus Russovisky", "39.946.558-9", "239.027.187-79",
				"matheusj@ozcorp.com", "123!", "6248973");

		Funcionario funcionario2 = new Funcionario(
				new Departamento("Departamento de Contabilidade", "DC", new Cargo("Gerente", 9237.00)), Sexo.FEMININO,
				TipoSanguineo.AB_POSITIVO, 2, "Sheila Absum", "64.159.964-7", "823.910.965-24", "sheila.asm@ozcorp.com",
				"567!", "8524763");

		Funcionario funcionario3 = new Funcionario(
				new Departamento("Departamento Administrativo", "DADM", new Cargo("Diretora", 18794.00)), Sexo.FEMININO,
				TipoSanguineo.A_NEGATIVO, 0, "Miriam Livnovsky", "54.239.714-2", "312.156.986-91",
				"miriamlili@ozcorp.com", "890!", "1358741");

		Funcionario funcionario4 = new Funcionario(
				new Departamento("Departamento Administrativo", "DADM", new Cargo("Secret�ria", 12521.00)),
				Sexo.FEMININO, TipoSanguineo.AB_NEGATIVO, 1, "Elesis Lire", "61.256.568-4", "841.543.859-61",
				"liregc@ozcorp.com", "327!", "0214869");

		Funcionario funcionario5 = new Funcionario(
				new Departamento("Departamento de Jogos", "DJ", new Cargo("Engenheiro", 11727.00)), Sexo.MASCULINO,
				TipoSanguineo.B_NEGATIVO, 3, "Kerin Eeveliant", "22.541.224-5", "889.445.921-30", "eevelike@ozcorp.com",
				"258!", "1548745");

		// Imprime dados
		System.out.println("***************************");
		System.out.println("* Dados dos Funcion�rios  *");
		System.out.println("***************************");

		System.out.println("Nome:                  " + funcionario1.getNome());
		System.out.println("RG:                    " + funcionario1.getRg());
		System.out.println("CPF:                   " + funcionario1.getCpf());
		System.out.println("Matr�cula:             " + funcionario1.getMatricula());
		System.out.println("E-mail:                " + funcionario1.getEmail());
		System.out.println("Senha:                 " + funcionario1.getSenha());
		System.out.println("Cargo:                 " + funcionario1.getDepartamento().cargo.titulo);
		System.out.println("Departamento:          " + funcionario1.getDepartamento().nome);
		System.out.println("Sigla:                 " + funcionario1.getDepartamento().sigla);
		System.out.println("Salario Bruto:         " + funcionario1.getDepartamento().cargo.salarioBase);
		System.out.println("Tipo Sangu�neo:        " + funcionario1.getTipoSanguineo().sangue);
		System.out.println("Sexo:                  " + funcionario1.getSexo().nomeDoSexo);

		System.out.println();
		System.out.println("========================================================================");
		System.out.println();

		System.out.println("Nome:                  " + funcionario2.getNome());
		System.out.println("RG:                    " + funcionario2.getRg());
		System.out.println("CPF:                   " + funcionario2.getCpf());
		System.out.println("Matr�cula:             " + funcionario2.getMatricula());
		System.out.println("E-mail:                " + funcionario2.getEmail());
		System.out.println("Senha:                 " + funcionario2.getSenha());
		System.out.println("Cargo:                 " + funcionario2.getDepartamento().cargo.titulo);
		System.out.println("Departamento:          " + funcionario2.getDepartamento().nome);
		System.out.println("Sigla:                 " + funcionario2.getDepartamento().sigla);
		System.out.println("Salario Bruto:         " + funcionario2.getDepartamento().cargo.salarioBase);
		System.out.println("Tipo Sangu�neo:        " + funcionario2.getTipoSanguineo().sangue);
		System.out.println("Sexo:                  " + funcionario2.getSexo().nomeDoSexo);

		System.out.println();
		System.out.println("========================================================================");
		System.out.println();

		System.out.println("Nome:                  " + funcionario3.getNome());
		System.out.println("RG:                    " + funcionario3.getRg());
		System.out.println("CPF:                   " + funcionario3.getCpf());
		System.out.println("Matr�cula:             " + funcionario3.getMatricula());
		System.out.println("E-mail:                " + funcionario3.getEmail());
		System.out.println("Senha:                 " + funcionario3.getSenha());
		System.out.println("Cargo:                 " + funcionario3.getDepartamento().cargo.titulo);
		System.out.println("Departamento:          " + funcionario3.getDepartamento().nome);
		System.out.println("Sigla:                 " + funcionario3.getDepartamento().sigla);
		System.out.println("Salario Bruto:         " + funcionario3.getDepartamento().cargo.salarioBase);
		System.out.println("Tipo Sangu�neo:        " + funcionario3.getTipoSanguineo().sangue);
		System.out.println("Sexo:                  " + funcionario3.getSexo().nomeDoSexo);

		System.out.println();
		System.out.println("========================================================================");
		System.out.println();

		System.out.println("Nome:                  " + funcionario4.getNome());
		System.out.println("RG:                    " + funcionario4.getRg());
		System.out.println("CPF:                   " + funcionario4.getCpf());
		System.out.println("Matr�cula:             " + funcionario4.getMatricula());
		System.out.println("E-mail:                " + funcionario4.getEmail());
		System.out.println("Senha:                 " + funcionario4.getSenha());
		System.out.println("Cargo:                 " + funcionario4.getDepartamento().cargo.titulo);
		System.out.println("Departamento:          " + funcionario4.getDepartamento().nome);
		System.out.println("Sigla:                 " + funcionario4.getDepartamento().sigla);
		System.out.println("Salario Bruto:         " + funcionario4.getDepartamento().cargo.salarioBase);
		System.out.println("Tipo Sangu�neo:        " + funcionario4.getTipoSanguineo().sangue);
		System.out.println("Sexo:                  " + funcionario4.getSexo().nomeDoSexo);

		System.out.println();
		System.out.println("========================================================================");
		System.out.println();

		System.out.println("Nome:                  " + funcionario5.getNome());
		System.out.println("RG:                    " + funcionario5.getRg());
		System.out.println("CPF:                   " + funcionario5.getCpf());
		System.out.println("Matr�cula:             " + funcionario5.getMatricula());
		System.out.println("E-mail:                " + funcionario5.getEmail());
		System.out.println("Senha:                 " + funcionario5.getSenha());
		System.out.println("Cargo:                 " + funcionario5.getDepartamento().cargo.titulo);
		System.out.println("Departamento:          " + funcionario5.getDepartamento().nome);
		System.out.println("Sigla:                 " + funcionario5.getDepartamento().sigla);
		System.out.println("Salario Bruto:         " + funcionario5.getDepartamento().cargo.salarioBase);
		System.out.println("Tipo Sangu�neo:        " + funcionario5.getTipoSanguineo().sangue);
		System.out.println("Sexo:                  " + funcionario5.getSexo().nomeDoSexo);

		System.out.println();
		System.out.println("========================================================================");
		System.out.println();

	}

}
