package br.com.ozcorp;

/**
 * 
 * @author Matheus Jos�
 *
 */

public class Funcionario {
	
	//Atributos
	Departamento departamento;
	Sexo sexo;
	TipoSanguineo tipoSanguineo;
	protected int nivelAcesso;
	protected String nome;
	protected String rg;
	protected String cpf;
	protected String email;
	protected String senha;
	protected String matricula;

	// Constructor
	protected Funcionario(Departamento departamento, Sexo sexo, TipoSanguineo tipoSanguineo, int nivelAcesso,
			String nome, String rg, String cpf, String email, String senha, String matricula) {
		super();
		this.departamento = departamento;
		this.sexo = sexo;
		this.tipoSanguineo = tipoSanguineo;
		this.nivelAcesso = nivelAcesso;
		this.nome = nome;
		this.rg = rg;
		this.cpf = cpf;
		this.email = email;
		this.senha = senha;
		this.matricula = matricula;
	}

	// Get & Set
	protected Departamento getDepartamento() {
		return departamento;
	}

	protected void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}

	protected Sexo getSexo() {
		return sexo;
	}

	protected void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}

	protected TipoSanguineo getTipoSanguineo() {
		return tipoSanguineo;
	}

	protected void setTipoSanguineo(TipoSanguineo tipoSanguineo) {
		this.tipoSanguineo = tipoSanguineo;
	}

	protected int getNivelAcesso() {
		return nivelAcesso;
	}

	protected void setNivelAcesso(int nivelAcesso) {
		this.nivelAcesso = nivelAcesso;
	}

	protected String getNome() {
		return nome;
	}

	protected void setNome(String nome) {
		this.nome = nome;
	}

	protected String getRg() {
		return rg;
	}

	protected void setRg(String rg) {
		this.rg = rg;
	}

	protected String getCpf() {
		return cpf;
	}

	protected void setCpf(String cpf) {
		this.cpf = cpf;
	}

	protected String getEmail() {
		return email;
	}

	protected void setEmail(String email) {
		this.email = email;
	}

	protected String getSenha() {
		return senha;
	}

	protected void setSenha(String senha) {
		this.senha = senha;
	}

	protected String getMatricula() {
		return matricula;
	}

	protected void setMatricula(String matricula) {
		this.matricula = matricula;
	}

}
